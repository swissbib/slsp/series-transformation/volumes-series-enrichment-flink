ThisBuild / resolvers ++= Seq(
  "Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/",
  "marcxml-fields" at "https://gitlab.com/api/v4/projects/12974592/packages/maven",
  Resolver.mavenLocal
)

name := "volumes-series-enrichment-flink"

version := "1.0-SNAPSHOT"

organization := "org.swissbib.slsp"

ThisBuild / scalaVersion := "2.12.12"

val flinkVersion = "1.10.1"

val dependencies = Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-table-planner" % flinkVersion,
  "org.apache.flink" %% "flink-table-api-scala-bridge" % flinkVersion,
  "org.scala-lang.modules" %% "scala-xml" % "1.3.0",
  "org.swissbib.slsp" % "marcxml-fields" % "0.8.1",
  "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test)

lazy val root = (project in file("."))
  .settings(
    libraryDependencies ++= dependencies
  )

assembly / mainClass := Some("org.swissbib.slsp.Job")

// make run command include the provided dependencies
Compile / run := Defaults.runTask(Compile / fullClasspath,
  Compile / run / mainClass,
  Compile / run / runner
).evaluated

// stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
Compile / run / fork := true
Global / cancelable := true

// exclude Scala library from assembly
assembly / assemblyOption := (assembly / assemblyOption).value.copy(includeScala = false)
