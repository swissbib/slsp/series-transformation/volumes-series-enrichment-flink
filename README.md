# Volumes Metadata Enrichment Based on Apache Flink

The application enriches bibliographic records with data found in related
records and removes redundant information. Relations of interest are series
<-> volumes as well as types of linking entries, namely supplement parent
entry <-> supplement/special issue entry and preceding entry <-> succeeding
entry.

It performs the following chores:

* Enriching volumes with information found in series it belongs to. For that
  purpose it uses the value found in subfield `839 $w`, which is equal to one
of the local system numbers (`035`) of the related series. The gathered series
data goes into the volume's newly built fields `800` (series added entry -
personal name), `810` (series added entry - corporate name), `811` (series
added entry - meeting name), and `830` (series added entry - uniform title).
* Enriching supplement/special issue children with information from their
  respective parent record.
* Enriching preceding records with information on their succeeding records.
* Removing fields `760` (main series entry), `765` (original language entry),
  `770` (supplement/special issue entry), `772` (supplement parent entry),
`773` (host item entry), `775` (other edition entry), `776` (additional
physical form entry), `777` (issued with entry), `780` (preceding entry),
`785` (succeeding entry), and `787` (other relationship entry) which point to
same record in a redundant fashion. This is done by dereferencing an existing
local system number of the related record.


## Running the application

* To run and test your application locally, you can just execute `sbt run` then select the main class that contains the Flink job: `org.swissbib.slsp.Job`
* For productive use create a fat jar with `sbt assembly` (run the command in the root
  folder of the project). Then upload the artifact (`target/scala-2.12/volumes-series-enrichment-flink-assembly-<version>.jar`) to your Flink cluster. This can either be done via Flink's UI or on the command line by running `flink run -c org.swissbib.slsp.Job /path/to/the/jar <arguments>`.

In either way you have to provide the following arguments:

* `-i /path/to/the/folder/containing/the/input/files`
* `-o /path/to/the/folder/where/the/enriched/files/go`

Furthermore it is recommended to set a high parallelism if possible (we
normally use 9 in a cluster with 12 task slots evenly distributed on 3 task
managers).

## Design draft (outdated as of September 2020)

![flowchart](docs/flowchart.png)
