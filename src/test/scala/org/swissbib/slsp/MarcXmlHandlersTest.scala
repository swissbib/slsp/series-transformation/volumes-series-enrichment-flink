/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import org.scalatest.flatspec.AnyFlatSpec

class MarcXmlHandlersTest extends AnyFlatSpec with MarcXmlHandlers {


  /*  "Extracting 7xx fields" should "result in a list of same size of Field7xx objects" in {
      val record: Elem =
        <record>
          <controlfield tag="001">324324323</controlfield>
          <datafield tag="785" ind1=" " ind2="0">
            <subfield code="i">Zwischenzeitlich 1860-1866 siehe</subfield>
            <subfield code="t">Würzburger medicinische Zeitschrift</subfield>
          </datafield>
          <datafield tag="785" ind1=" " ind2="0">
            <subfield code="t">Berichte der Physikalisch-Medizinischen Gesellschaft zu Würzburg</subfield>
            <subfield code="w">123456789</subfield>
          </datafield>
        </record>

      val expectedResult = List(
        RichField7xx("785",
          "324324323",
          Seq("234234"),
          "blabla",
          None,
          <datafield tag="785" ind1=" " ind2="0">
            <subfield code="i">Zwischenzeitlich 1860-1866 siehe</subfield>
            <subfield code="t">Würzburger medicinische Zeitschrift</subfield>
          </datafield>),
        RichField7xx("785",
          "324324323",
          Seq("234234"),
          "blabla",
          Some("123456789"),
          <datafield tag="785" ind1=" " ind2="0">
            <subfield code="t">Berichte der Physikalisch-Medizinischen Gesellschaft zu Würzburg</subfield>
            <subfield code="w">123456789</subfield>
          </datafield>)
      )

      assert(extract7xxFields(record) === expectedResult)
    } */

 /* "Merging same 7xx fields" should "result in the replacement of the original 7xx fields" in {
    val origRecord =
    {<record>
        <controlfield tag="001">324324323</controlfield>
        <datafield tag="785" ind1=" " ind2="0">
          <subfield code="i">Zwischenzeitlich 1860-1866 siehe</subfield>
          <subfield code="t">Würzburger medicinische Zeitschrift</subfield>
        </datafield>
        <datafield tag="785" ind1=" " ind2="0">
          <subfield code="t">Berichte der Physikalisch-Medizinischen Gesellschaft zu Würzburg</subfield>
          <subfield code="w">123456789</subfield>
        </datafield>
      </record>}

    val newFields = Seq(
      {<datafield tag="785" ind1=" " ind2="0">
        <subfield code="i">Neu 1</subfield>
        <subfield code="t">Würzburger medicinische Zeitschrift</subfield>
      </datafield>},
      {<datafield tag="785" ind1=" " ind2="0">
        <subfield code="t">Neu 2</subfield>
        <subfield code="w">123456789</subfield>
      </datafield>}
    )

    val resultingRecord =
    {<record>
        <controlfield tag="001">324324323</controlfield>
        <datafield tag="785" ind1=" " ind2="0">
          <subfield code="i">Neu 1</subfield>
          <subfield code="t">Würzburger medicinische Zeitschrift</subfield>
        </datafield>
        <datafield tag="785" ind1=" " ind2="0">
          <subfield code="t">Neu 2</subfield>
          <subfield code="w">123456789</subfield>
        </datafield>
      </record>}

    assert(mergeField7xx(origRecord, newFields) === resultingRecord)
  }*/

}
