/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import org.scalatest.flatspec.AnyFlatSpec

import scala.xml.Elem

class AccumulatorsTest extends AnyFlatSpec {

  def fixture = new {
    val field7xxAccumulator = new Field7xxAccumulator
    val field8xxAccumulator = new Field8xxAccumulator
    val elem1: Elem = <record>blablabla</record>
    val elem2: Elem = <record>blublublu</record>
  }

  /*
  "Adding a unique token in Field7xxAccumulator" should "add a new object" in {
    val f = fixture
    f.field7xxAccumulator.add(("12345", "6789"), "aRecId", fixture.elem1)
    f.field7xxAccumulator.add(("12345", "6789"), "aRecId", fixture.elem1)
    f.field7xxAccumulator.add(("abcd", "efgh"), "aRecId", fixture.elem2)
    assert(f.field7xxAccumulator.getKeyedField === KeyedField("aRecId", Seq(f.elem1, f.elem2)))
  } */

  /* "Adding a unique token in Field8xxAccumulator" should "add a new object" in {
     val f = fixture
     f.field8xxAccumulator.add("12345", "aRecId", (fixture.elem1, "awef", "awef"))
     f.field8xxAccumulator.add("12345", "aRecId", (fixture.elem1, "awef", "awef"))
     f.field8xxAccumulator.add("6789", "aRecId", (fixture.elem2, "afaewf", "afefwa"))
     assert(f.field8xxAccumulator.getKeyedField === KeyedField("aRecId", Seq(f.elem1, f.elem2)))
   }*/


}
