/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import org.swissbib.slsp.UtilityTypes._

import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, NodeSeq, XML}

trait MarcXmlHandlers {

  /**
    * Parses line as MARC-XML record
    *
    * @param line line in file
    * @return MARC-XML record
    */
  protected def parseRecord(line: String): Elem = XML.loadString(line)

  /**
    * Serializes MARC-XML record as String
    *
    * @param record MARC-XML record
    * @return String
    */
  protected def serializeRecord(record: Elem): String = record.toString()

  /**
    * Gets record id as defined in Marc field 001
    *
    * @param xml MARC-XML record root element
    * @return Record id
    */
  protected def getField001Content(xml: Elem): String =
    (xml \\ "controlfield")
      .find(cf => (cf \@ "tag") == "001")
      .get
      .text

  /**
    * Gets Seq of control numbers as defined in Marc field 035$a
    *
    * @param xml Marc XML record root element
    * @return List of control numbers
    */
  protected def getField035Contents(xml: Elem): Seq[String] =
    getRField(xml)("035").map(getNRSubfieldContent(_)("a")).map(t => t.get)

  /**
    * Gets content of field 245$a (i.e. title of record)
    *
    * @param xml MARC-XML record root element
    * @return Title of record
    */
  protected def getField245aContent(xml: Elem): String = {
    (xml \\ "datafield").find(df => (df \@ "tag") == "245") match {
      case Some(f) => getNRSubfieldContent(f)("a").getOrElse("")
      case None => ""
    }
  }

  /**
    * Gets content of field 245$n (i.e. part of title)
    *
    * @param xml MARC-XML record root element
    * @return Part of title
    */
  protected def getField245nContent(xml: Elem): List[String] = {
    (xml \\ "datafield").find(df => (df \@ "tag") == "245") match {
      case Some(f) => getRSubfieldContent(f)("n")
      case None => List()
    }
  }


  /**
    * Gets content of field 245$p (i.e. part of title)
    *
    * @param xml MARC-XML record root element
    * @return Part of title
    */
  protected def getField245pContent(xml: Elem): List[String] = {
    (xml \\ "datafield").find(df => (df \@ "tag") == "245") match {
      case Some(f) => getRSubfieldContent(f)("p")
      case None => List()
    }
  }

  /**
    * Checks if field 839 exists in record
    *
    * @param xml Marc-XML record
    * @return true if field exists
    */
  protected def hasFields839(xml: Elem): Boolean = getRField(xml)("839").nonEmpty

  /**
    * Checks if field contains subfield code
    *
    * @param xml          field
    * @param subfieldCode subfield code
    * @return true if subfield code exists in field
    */
  protected def hasSubfield(xml: Elem, subfieldCode: String): Boolean =
    (xml \\ "subfield").exists(sf => sf \@ "code" == subfieldCode)

  /**
    * Gets content of subfields 839$v, 839$w and 839$6
    *
    * @param xml Marc XML record root element
    * @return List of control numbers
    */
  protected def getSubfields839vw6Contents(xml: Elem): Seq[(String, String, String)] = {
    getRField(xml)("839")
      .map(f => (getNRSubfieldContent(f)("v"), getNRSubfieldContent(f)("w"), getNRSubfieldContent(f)("6")))
      .map(x => {
        val sfList = x.productIterator.asInstanceOf[Iterator[Option[String]]].map {
          case Some(x) => x
          case None => ""
        }.toList
        (sfList.head, sfList(1), sfList(2))
      })
  }

  /**
    * Gets content of repeatable datafield as `NodeSeq`
    *
    * @param rootElem  entire record as `Elem`
    * @param fieldName field name (tag) to be extracted
    * @return `NodeSeq` of datafields
    */
  protected def getRField(rootElem: Elem)(fieldName: String): NodeSeq =
    (rootElem \\ "datafield").filter(df => df \@ "tag" == fieldName)

  /**
    * Gets content of a non-repeatable subfield
    *
    * @param field        field as XML `Node`
    * @param subfieldName subfield code
    * @return optional content
    */
  protected def getNRSubfieldContent(field: Node)(subfieldName: String): Option[String] =
    (field \\ "subfield").find(s => s \@ "code" == subfieldName) match {
      case Some(v) => Some(v.text)
      case _ => None
    }

  /**
    * Gets content of a repeatable subfield
    *
    * @param field        field as XML `Node`
    * @param subfieldName subfield code
    * @return list of subfield values
    */
  protected def getRSubfieldContent(field: Node)(subfieldName: String): List[String] =
    (field \\ "subfield").filter(sf => sf \@ "code" == subfieldName).map(c => c.text).toList

  private val addChildNode: String => Seq[Elem] => RewriteRule = parentTag => e => new RewriteRule {
    override def transform(n: Node): Seq[Node] = n match {
      case elem: Elem if elem.label == parentTag =>
        elem.copy(child = elem.child ++ e)
      case x => x
    }
  }

  /**
    * Adds a sequence of fields to record (used as a transformation rule)
    */
  protected val addFieldToRecord: Seq[Elem] => RewriteRule =
    addChildNode("record")

  /**
    * Adds a sequence of subfields to field (used as a transformation rule)
    */
  protected val addSubfieldToDatafield: Seq[Elem] => RewriteRule =
    addChildNode("datafield")

  /**
    * Filters out fields based on tag name of a `Seq[Elem]`
    *
    * @param filters filters to apply (fields with same tag and indicators will be filtered out)
    * @param field   record on which the filter should be applied
    * @return cleaned record
    */
  protected def filterDatafields(filters: Seq[Elem])(field: Node): Boolean =
    filters.exists(f => (field \ "@tag").text == (f \ "@tag").text)

  /**
    * Merges 8xx fields with record
    *
    * @param rec      entire record
    * @param field8xx 8xx fields to be merged
    * @return enriched record
    */
  protected def mergeField8xx(rec: Elem, field8xx: Seq[Elem]): Elem = {
    val transform = new RuleTransformer(addFieldToRecord(field8xx))
    transform(rec) match {
      case e: Elem => e
      case _ => rec
    }
  }

  /**
    * Merges 7xx fields with record
    *
    * @param rec      entire record
    * @param field7xx 7xx fields to be merged
    * @return enriched record
    */
  protected def mergeField7xx(rec: Elem, field7xx: Seq[Elem]): Elem = {
    //@formatter:off
    val reducedRec = <record>{rec.flatMap(_.child).filterNot(filterDatafields(field7xx)(_))}</record>
    //@formatter:on
    val add = new RuleTransformer(addFieldToRecord(field7xx))
    add(reducedRec) match {
      case e: Elem => e
      case _ => rec
    }
  }

  protected def remove839Fields(rec: Elem): Elem = {
    //@formatter:off
    val field839: Elem = {<datafield tag="839" ind1=" " ind2="0"></datafield>}
    <record>{rec.flatMap(_.child).filterNot(filterDatafields(Seq(field839))(_))}</record>
    //@formatter:on
  }

  /**
    * Extracts fields with tag 760, 765, 770, 772, 773, 775, 776, 777, 780, 785, and 787 from record
    *
    * @param rec record
    * @return List of fields
    */
  protected def extract7xxFields(rec: Elem): Seq[RichField7xx] =
    Seq("760", "765", "770", "772", "773", "775", "776", "777", "780", "785", "787")
      .foldLeft[Seq[RichField7xx]](Seq())((acc, field) => acc ++ buildRich7xxField(rec, field))

  /**
    * Get id of belonging series from field 7xx##$w in volume record
    *
    * @param volume    volume record
    * @param fieldName field name (tag)
    * @return value found in subfield $w
    */
  private def buildRich7xxField(volume: Elem, fieldName: String): Seq[RichField7xx] = {
    val recId = getField001Content(volume)
    val fields035 = getField035Contents(volume)
    val titleMain = getField245aContent(volume)
    val titlePartNumber = getField245nContent(volume)
    val titlePart = getField245pContent(volume)
    getRField(volume)(fieldName)
      .flatMap {
        case field@(_: Elem) => Some(RichField7xx(fieldName, recId, fields035, titleMain, titlePartNumber, titlePart, getNRSubfieldContent(field)("w"), field))
        case _ => None
      }
  }

  def addSubfields(field: Elem, subfieldCode: String, subfieldValues: Seq[String]): Elem = {
    //@formatter:off
    val subfields = subfieldValues.map(sV => <subfield code={subfieldCode}>{sV}</subfield>)
    //@formatter:on
    val transform = new RuleTransformer(addSubfieldToDatafield(subfields))
    transform(field) match {
      case e: Elem => e
      case _ => field
    }
  }

  def buildFields772780(recId: String, titleMain: String, titlePartNumbers: Seq[String], titleParts: Seq[String], fieldTag: String, field7xx: Elem): Seq[Field772780] =
    (field7xx \\ "subfield")
      .filter(sf => sf \@ "code" == "w")
      .map(sf => Field772780(recId, titleMain, titlePartNumbers, titleParts, fieldTag, sf.text))

  def getSubfieldtHashCode(recId: String, tag: String, field: Elem): Int = {
    (recId + tag + getNRSubfieldContent(field)("t").getOrElse("")).hashCode
  }

  def getKeyedField(id: String, records: Seq[(VolumeRecord, SeriesRecord)]): KeyedField = {
    val elems: Seq[Elem] = records
      .flatMap(e => {
        val concatenatedFields = NewFields.field800(e._1.sf839v, e._1.seriesId, e._1.sf8396).buildFrom(e._2.record).getOrElse(Seq()) ++
          NewFields.field810(e._1.sf839v, e._1.seriesId, e._1.sf8396).buildFrom(e._2.record).getOrElse(Seq()) ++
          NewFields.field811(e._1.sf839v, e._1.seriesId, e._1.sf8396).buildFrom(e._2.record).getOrElse(Seq())
        if (concatenatedFields.isEmpty) {
          concatenatedFields ++ NewFields.field830(e._1.sf839v, e._1.seriesId, e._1.sf8396).buildFrom(e._2.record).getOrElse(Seq())
        }
        else {
          concatenatedFields
        }
      })
      .map(e => e.asInstanceOf[Elem])
    KeyedField(id, elems)
  }
}
