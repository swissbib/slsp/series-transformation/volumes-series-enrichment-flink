/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import org.apache.flink.util.Collector
import org.swissbib.slsp.UtilityTypes._

import scala.xml.Elem

trait Transformators extends MarcXmlHandlers {
  /**
    * Checks if line is XML record
    *
    * @param record Potential record
    * @return true if XML record
    */
  def isRecord(record: String): Boolean = record.startsWith("<record>")

  /**
    * Checks for existence of subfield 839##$w, which indicates a volume record
    *
    * @param record MARC-XML record
    * @return true if subfield exists
    */
  def isVolumeRecord(record: Elem): Boolean = hasFields839(record)

  /**
    * Creates a `SeriesRecord` out of a MARC-XML record
    *
    * @param record MARC-XML record
    * @return a new `SeriesRecord`
    */
  def createSeriesRecords(record: Elem): Seq[SeriesRecord] = {
    getField035Contents(record)
      .map(field035 => SeriesRecord(getField001Content(record), field035, record))
  }

  /**
    * Creates a sequence of `StrippedVolumeRecord`s out of a MARC-XML record
    *
    * @param record MARC-XML record
    * @return sequence of `StrippedVolumeRecord`s
    */
  def createStrippedVolumeRecords(record: Elem): Seq[VolumeRecord] =
    getSubfields839vw6Contents(record)
      .map(sf839vw6 => VolumeRecord(getField001Content(record), sf839vw6._2, sf839vw6._1, sf839vw6._3))

  /**
    * Applies a merge function on a MARC-XML record and a sequence of fields with the same tag (`KeyedField`)
    *
    * @param rec      MARC-XML record
    * @param field    sequence of fields with the same tag
    * @param mergeFun merge function
    * @return enriched MARC-XML record
    */
  def applyMerge(rec: Elem, field: KeyedField)(mergeFun: (Elem, Seq[Elem]) => Elem): Elem =
    if (field == null) {
      rec
    } else {
      mergeFun(rec, field.fields)
    }

  def reduce7xxFields(fields: Iterator[(String, Seq[SlimField7xx])], out: Collector[(String, Seq[SlimField7xx])]): Unit = {
    val aggregation = fields.flatMap(_._2).foldLeft(new Field7xxAccumulator)((acc, x) => {
      acc.add((x.fieldTag, x.seriesId), x.recId, x)
    })
    out.collect((aggregation.getVolumeRecId, aggregation.getReducedRecord))
  }

  def reduce8xxFields(recs: Iterator[(String, Seq[(VolumeRecord, SeriesRecord)])], out: Collector[(String, Seq[(VolumeRecord, SeriesRecord)])]): Unit = {
    val aggregation = recs.flatMap(_._2).foldLeft(new Field8xxAccumulator)((acc, x) => {
      acc.add(x._2.recId, x._1.recId, (x._1, x._2))
    })
    out.collect((aggregation.getVolumeRecId, aggregation.getReducedRecord))
  }

  def splitFieldIntoLinked035Instances(field: RichField7xx): Seq[(Int, RichField7xx)] = {
    val tempId = (field.recId ++ field.field7xxContent.toString).hashCode
    field.fields035.map(f035 =>
      (tempId, RichField7xx(field.fieldTag,
        field.recId,
        Seq(f035),
        field.titleMain,
        field.titlePartNumbers,
        field.titleParts,
        field.seriesId,
        field.field7xxContent)))

  }

  def isField770or785WithoutwSubfield(field: RichField7xx): Boolean =
    (field.fieldTag == "770" || field.fieldTag == "785") && !hasSubfield(field.field7xxContent, "w")

  def linkToRelatedRecord(left: (Int, RichField7xx), right: Field772780): (Int, RichField7xx) = {
    if (right != null) {
      val updated7xxField =
        addSubfields(left._2.field7xxContent, "w", Seq(s"(swissbib)${right.recId}-41slsp_network"))
      (left._1, RichField7xx(left._2.fieldTag, left._2.recId, left._2.fields035, left._2.titleMain, left._2.titlePartNumbers, left._2.titleParts, left._2.seriesId, updated7xxField))
    } else left
  }

  def addMissingSubfieldw(left: RichField7xx, right: RichField7xx): RichField7xx =
    if ((left != null) &&
      hasSubfield(left.field7xxContent, "w") &&
      !hasSubfield(right.field7xxContent, "w"))
      left
    else
      right

  def replace035with001valueAndSlimDownField(field7xx: RichField7xx, lookup: LocalKeyRecKeyEntry): (String, Seq[SlimField7xx]) = {
    (field7xx.recId,
      Seq(SlimField7xx(field7xx.fieldTag,
        field7xx.recId,
        if (lookup != null) lookup.recId else generateRandomHash,
        field7xx.field7xxContent))
    )
  }

  /**
    * Extract and normalise 770$t / 785$t for a comparison with 245$a and 245$p.
    *
    * @param field field 770 / 785
    * @return stripped and split content
    */
  def extractAndNormaliseTitleFrom770785(field: Elem): String =
    getNRSubfieldContent(field)("t") match {
      case Some(t) =>
        t.split(" / ")(0)
          .replaceAll("[-<>., ]", "")
          .toLowerCase
      case None => ""
    }

  /**
    * Normalise 245$a, 245$n and 245$p for a comparison with 770$t or 785$t
    *
    * @param titleMain        value of subfield 245$a
    * @param titlePartNumbers value of subfield 245$n
    * @param titleParts       value of subfield 245$p
    * @return
    */
  def normaliseTitleFrom245anp(titleMain: String, titlePartNumbers: Seq[String], titleParts: Seq[String]): String = {
    (titleMain + titlePartNumbers.mkString("") + titleParts.mkString("")).split(" / ")(0)
      .replaceAll("[-<>., ]", "")
      .toLowerCase
  }

  private def generateRandomHash: String = {
    val sample = (Range('0', '9') ++ Range('A', 'Z') ++ Range('a', 'z')).toList
    Range(0, 10).map(_ => sample((scala.util.Random.nextInt() % sample.size).abs).toChar).mkString
  }
}
