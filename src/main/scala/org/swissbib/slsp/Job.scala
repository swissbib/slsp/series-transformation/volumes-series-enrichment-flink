/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp


import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.apache.flink.api.common.operators.base.JoinOperatorBase.JoinHint
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.api.scala._
import org.swissbib.slsp.UtilityTypes._

//noinspection ConvertibleToMethodValue
object Job extends App with Utils with Transformators {

  /*
  Step 0: Argument parsing
   */

  val pt = ParameterTool.fromArgs(args)
  val inputFilePath = getArgumentValue("i", "input")(pt)
  if (inputFilePath.isEmpty) {
    println("You have to provide a path to a file or directory containing the input data. Use flag -i <path> / --input <path>")
    sys.exit(1)
  }
  val outputFilePath = getArgumentValue("o", "output")(pt)
  if (outputFilePath.isEmpty) {
    println("You have to provide a path to a file or directory containing the output data. Use flag -o <path> / --output <path>")
    sys.exit(1)
  } else if (!new File(outputFilePath.get).isDirectory) {
    new File(outputFilePath.get).mkdirs()
  }
  val now = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now())
  val fileSink = new FileSink(outputFilePath.get, 200000, now)


  /*
  Step 1: Parse MARC-XML records
   */

  val env = ExecutionEnvironment.getExecutionEnvironment
  env.getConfig.setGlobalJobParameters(pt)
  val xmlRecords = env.readTextFile(inputFilePath.get)
    .filter(isRecord _)
    .map(parseRecord _)


  /*
  Step 2: Use MARC-XML records to build intermediary series and volume records; join records with matching local
  system numbers
   */

  val volumes = xmlRecords.filter(isVolumeRecord _)
    .flatMap(createStrippedVolumeRecords _)
  val series = xmlRecords.flatMap(createSeriesRecords _)
  val volumesSeriesJoin = volumes.join(series, JoinHint.BROADCAST_HASH_FIRST)
    .where("seriesId")
    .equalTo("localRecId") {
      (volume, series) => (volume.recId, Seq((volume, series)))
    }


  /*
  Step 3: Build fields 800, 810, 811 and 830 by merging content of related series and volume records.
  Build only one field 8xx stemming from a particular series-volume-relation. For the comprehensive extraction
  ruleset see https://gitlab.com/swissbib/slsp/series-transformation/kafka-streams-series-transform/issues/3
  */

  val fields8xx = volumesSeriesJoin
    .groupBy(_._1)
    .reduceGroup(reduce8xxFields _)
    .map(x => getKeyedField(x._1, x._2))


  /*
  Step 4 does two things to consolidate information in 7xx fields. First, it removes redundant 760, 765, 770,
  772, 773, 775, 776, 777, 780, 785 and 787 fields. Redundancy in this case means that the respective local
  system control number of the linked record (035$a), which is contained in 7xx$w, resolves to the same record
  (i.e. record with main system control number in field 001). Second, the fields 770 (Supplement/Special Issue Entry)
  and 785 (Succeeding Entry) are enriched with the main system control number of the record which already links
  to one of the local system control numbers of the former one in field 772 (Supplement Parent Entry) or 780
  (Preceding Entry), thus making the link between the two records reciprocal.

  Substep 4a: Extract 7xx fields (these fields are further processed in step 4b) and "double link" 770<->772 and
  785<->780.
  */

  val originalFields7xx = xmlRecords.flatMap(extract7xxFields _)

  val fields772780: DataSet[Field772780] = originalFields7xx
    .filter(field7xx => field7xx.fieldTag == "772" || field7xx.fieldTag == "780")
    .flatMap(f => buildFields772780(f.recId, f.titleMain, f.titlePartNumbers, f.titleParts, f.fieldTag, f.field7xxContent))

  val fields770785 = originalFields7xx
    .filter(isField770or785WithoutwSubfield _)
    .flatMap(splitFieldIntoLinked035Instances _)
    .leftOuterJoin(fields772780)
    .where(f => (f._2.fields035.head, extractAndNormaliseTitleFrom770785(f._2.field7xxContent)))
    .equalTo(f => (f.sf7xxw, normaliseTitleFrom245anp(f.titleMain, f.titlePartNumbers, f.titleParts)))
    .apply(linkToRelatedRecord _)
    .groupBy(0)
    .reduce((agg, x) => if (hasSubfield(x._2.field7xxContent, "w")) x else agg)
    .map(t => t._2)

  val updatedRichFields7xx = fields770785
    .rightOuterJoin(originalFields7xx)
    .where(l => getSubfieldtHashCode(l.recId, l.fieldTag, l.field7xxContent))
    .equalTo(r => getSubfieldtHashCode(r.recId, r.fieldTag, r.field7xxContent))
    .apply(addMissingSubfieldw _)

  /*
  Substep 4b: Reduce 7xx fields pointing to the same series record (i.e. subfield $w ->
  field 035 of series record) to a single instance. This removes redundant information.
   */

  val localKeyRecKeyMap = series.map(s => LocalKeyRecKeyEntry(s.localRecId, s.recId))

  val fields7xxWithSeriesRecIds: DataSet[(String, Seq[SlimField7xx])] = updatedRichFields7xx
    .leftOuterJoin(localKeyRecKeyMap)
    .where(_.seriesId.getOrElse(""))
    .equalTo("localRecId") {
      replace035with001valueAndSlimDownField _
    }

  val fields7xx = fields7xxWithSeriesRecIds
    .groupBy(0)
    .reduceGroup(reduce7xxFields _)
    .map(x => KeyedField(x._1, x._2.map(_.field7xxContent).toList))


  /*
  Step 5: Replace 839 with 800, 810, 811 or 830 fields (see step 3) and 7xx fields with its reduced form (see step 4)
  in each record
   */

  val recordsWith8xxFields = xmlRecords
    .map(remove839Fields _)
    .leftOuterJoin(fields8xx, JoinHint.REPARTITION_HASH_SECOND)
    .where(getField001Content _)
    .equalTo(_.recId)
    .apply(applyMerge(_, _)(mergeField8xx))

  val finalRecords = recordsWith8xxFields
    .leftOuterJoin(fields7xx, JoinHint.REPARTITION_HASH_SECOND)
    .where(getField001Content _)
    .equalTo(_.recId)
    .apply(applyMerge(_, _)(mergeField7xx))


  /*
  Step 6: Change order of tags in datafields and write records to compressed files
   */

  finalRecords.map(serializeRecord _)
    .map(s => s.replaceAll("ind2=\"([^\"]+)\" ind1=\"([^\"]+)\" tag=\"([^\"]+)\"", "tag=\"$3\" ind1=\"$2\" ind2=\"$1\""))
    .output(fileSink)


  env.execute("Volumes Series Enrichment")
}