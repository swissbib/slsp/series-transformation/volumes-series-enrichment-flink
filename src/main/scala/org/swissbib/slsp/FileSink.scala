/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import java.io.{FileOutputStream, PrintWriter}
import java.util.zip.GZIPOutputStream

import org.apache.flink.api.common.io.OutputFormat
import org.apache.flink.configuration.Configuration

import scala.util.{Failure, Success, Try}

class FileSink(path: String, maxLineSize: Int, timeString: String) extends OutputFormat[String] {

  var lineCounter = 0
  var fileCounter = 0
  @transient var printer: PrintWriter = _
  var taskNumber: Int = _

  def this(path: String, timeString: String) {
    this(path, 200000, timeString)
  }

  override def configure(parameters: Configuration): Unit = {}

  override def open(taskNumber: Int, numTasks: Int): Unit = {
    this.taskNumber = taskNumber
  }

  override def writeRecord(record: String): Unit = {
    if (lineCounter == 0) {
      openNewFile()
    }
    lineCounter += 1
    printer.println(record)
    if (lineCounter >= maxLineSize) {
      closeFile()
      lineCounter = 0
    }
  }

  override def close(): Unit = {
    closeFile()
  }

  private def openNewFile(): Unit = {
    fileCounter += 1
    val fileName = f"out_${timeString}_$taskNumber%02d_$fileCounter%03d.xml.gz"
    val fullName = s"${if (path.endsWith("/")) path else path + "/"}$fileName"
    Try(new FileOutputStream(fullName))
      .flatMap((fos: FileOutputStream) => Try(new GZIPOutputStream(fos)))
      .flatMap(zipped => Try(new PrintWriter(zipped, true))) match {
      case Success(p) => printer = p
      case Failure(_) =>
    }
    printer.println("<?xml version=\"1.0\" encoding=\"utf-8\" ?>")
    printer.println("<collection>")
  }

  private def closeFile(): Unit = {
    if (printer != null) {
      printer.println("</collection>")
      printer.close()
    }
  }
}
