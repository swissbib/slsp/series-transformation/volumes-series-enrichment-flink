/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import org.swissbib.slsp.UtilityTypes.{SeriesRecord, SlimField7xx, VolumeRecord}

import scala.collection.mutable.ArrayBuffer

/**
  * Helper trait to accumulate fields with common tag in records
  *
  * @tparam T Type of unique key (used to filter out duplicate content)
  * @tparam U Type of records to be accumulated
  */
sealed trait Accumulator[T, U] {
  self =>
  private val uniqueKeys: ArrayBuffer[T] = ArrayBuffer()
  private var volumeRecId: Option[String] = None
  protected val records: ArrayBuffer[U] = ArrayBuffer()

  /**
    * Adds required information for accumulating a field (doesn't do something if `uniqueKey` is already registered in accumulator)
    *
    * @param uniqueKey   unique key
    * @param volumeRecId record id of volume
    * @param record      content of record
    * @return the updated instance of the accumulator
    */
  def add(uniqueKey: T, volumeRecId: String, record: U): self.type = {
    if (isUnique(uniqueKey)) {
      addUniqueKey(uniqueKey)
      addVolumeRecId(volumeRecId)
      addRecord(record)
    }
    this
  }

  /**
    * Gets id of volume record
    *
    * @return record id
    */
  def getVolumeRecId: String = volumeRecId match {
    case Some(v) => v
    case None => ""
  }

  /**
    * Returns aggregated fields as `KeyedField`
    *
    * @return KeyedField
    */
  //def getKeyedField: KeyedField

  def getReducedRecord: Seq[U] = records.toList

  private def addUniqueKey(key: T): Unit = uniqueKeys.append(key)

  private def isUnique(key: T): Boolean = !uniqueKeys.contains(key)

  private def addVolumeRecId(recId: String): Unit = if (recId.nonEmpty) this.volumeRecId = Some(recId)

  private def addRecord(record: U): Unit = this.records.append(record)

}

/**
  * Helper class to accumulate fields 7xx
  */
class Field7xxAccumulator extends Accumulator[(String, String), SlimField7xx] {}

/**
  * Helper class to accumulate fields 8xx
  */
class Field8xxAccumulator extends Accumulator[String, (VolumeRecord, SeriesRecord)] {}
