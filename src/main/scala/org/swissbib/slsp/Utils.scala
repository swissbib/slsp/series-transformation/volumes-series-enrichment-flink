/*
 * Flink workflow for enriching volume records
 * Copyright (C) 2019  project swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.slsp

import java.io.File

import org.apache.flink.api.java.utils.ParameterTool

/**
  * Provides utility methods for I/O and other operations
  */
trait Utils {
  /**
    * Extracts argument value (short and long option) of `ParameterTool` instance
    *
    * @param pt instance of `ParameterTool`
    * @return value as String, `None` if no indicated argument is available
    */
  protected def getArgumentValue(shortArg: String, longArg: String)(pt: ParameterTool): Option[String] = {
    (pt.has(shortArg), pt.has(longArg)) match {
      case (false, false) => None
      case (true, _) => Some(pt.get(shortArg))
      case (_, true) => Some(pt.get(longArg))
    }
  }

  /**
    * Returns gzipped XML files when path is given
    *
    * @param path path to directory
    * @return List of gzipped XML files in directory
    */
  protected def getXmlFileListInDirectory(path: String): Seq[String] = {
    val dir = new File(path)
    if (dir.exists && dir.isDirectory) {
      dir.listFiles().filter(_.isFile).filter(_.getName.endsWith("xml.gz")).map(_.getAbsolutePath).toList
    } else {
      Seq()
    }
  }
}
